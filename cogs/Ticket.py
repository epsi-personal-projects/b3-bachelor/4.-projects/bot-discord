from discord.ext import commands

class Ticket(commands.Cog):
    # Declaration of our self.bot that will represent the instance of our bot
    def __init__(self, bot):
        self.bot = bot


async def setup(bot):
    await bot.add_cog(Ticket(bot))