import asyncio
import os
import discord

from dotenv import load_dotenv
from models.Application import Application

# ---------------------------------------------------------------------------- #
#                                   PRE INIT                                   #
# ---------------------------------------------------------------------------- #

load_dotenv()  # load environment variables from .env.

# ---------------------------------------------------------------------------- #
#                               APPLICATION START                              #
# ---------------------------------------------------------------------------- #

#Creation of an instance of Application, in this case, it will represent our "bot"
bot = Application(command_prefix = "!", self_bot = False, intents = discord.Intents.all())

#Main will start by loading some parts of our application, then, it will start our bot.
async def main():
    async with bot:
        await bot.load_extensions()
        # With os.getenv we're able to get our environment variable declared in our .env
        await bot.start(os.getenv("BOT_DISCORD_TOKEN"))

asyncio.run(main())

# ---------------------------------------------------------------------------- #