FROM python:3.11

WORKDIR /home/bot-discord

#Install Python libraries
RUN pip install --upgrade pip
RUN python3 -m pip install -U discord.py
RUN pip install python-dotenv
RUN pip install pylint

#Install npm/node environment so we can watch our files with nodemon
RUN apt update -y && apt install npm -y && apt install nodejs -y
RUN npm i -g nodemon