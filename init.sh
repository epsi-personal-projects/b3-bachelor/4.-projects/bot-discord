# ---------------------------------------------------------------------------- #

# DO NOT RUN THIS SCRIPT IF YOU ARE USING DOCKER !
# chmod 700 ./init.sh

# ---------------------------------------------------------------------------- #

# Install Python libraries
pip install --upgrade pip
python3 -m pip install -U discord.py
pip install python-dotenv
pip install pylint

#Install npm/node environment so we can watch our files with nodemon
apt update -y && apt install npm -y && apt install nodejs -y
npm install -g nodemon