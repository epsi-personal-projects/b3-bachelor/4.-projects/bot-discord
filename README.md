# DISCORD BOT - EPSI
This is a discord bot with the objective of respecting the necessary parameters to validate a college project. It has several features that will be detailed later on.

# DEVELOPMENT ENVIRONMENT

## Pulling Repository

Chose any path in your system and clone the git repository

```
HTTP: 
git clone https://gitlab.com/epsi-personal-projects/b3-bachelor/4.-projects/bot-discord.git
```
```
SSH:
ssh-keygen -t rsa (Add the sshkey to your gitlab account)

git clone git@gitlab.com:epsi-personal-projects/b3-bachelor/4.-projects/bot-discord.git
```


## Running Docker with the bot
The first thing to do will be to create our custom docker image. (be sure you are inside the repository you just cloned)

```
docker build -t bot-discord:1.0 .
```

The command bellow will bound the repository that you just cloned with the volume inside the container.

```
docker run -dti -v $(pwd):/home/bot-discord --name bot-discord bot-discord:1.0
```

The command bellow will let you attach to the container that you just created

```
docker exec -ti bot-discord bash
```

## Running the bot in dev environment

The command bellow will let you run the bot, but, it will not restart the bot each time you save your project.
```
python3 main.py
```
The command bellow will let you run the bot and automatically restart each time you save anything.
```
nodemon main.py // npm start
```

