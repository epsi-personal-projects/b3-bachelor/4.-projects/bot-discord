import discord
from discord.ext import commands
import os

class Application(commands.Bot):
    _bot_starting_message = "[INFO]: Bot is now online"
    _bot_status = "Bot still online"

    _logs_channel = 1040163874789392414

    # Construct an instance of commands.Bot and then add trigger the add_commands method that will register some generic commands.
    def __init__(self, command_prefix, self_bot, intents):
        commands.Bot.__init__(self, command_prefix=command_prefix, self_bot=self_bot, intents=intents)
        self.add_commands()

    # on_ready is a discord-py event that will trigger when the bot has started
    async def on_ready(self):
        channel = self.get_channel(Application._logs_channel)
        await channel.send(Application._bot_starting_message)
        print(Application._bot_starting_message)

    # Load up every single cog inside their generic folder "./cogs"
    async def load_extensions(self):
        for filename in os.listdir("./cogs"):
            if filename.endswith(".py"):
                await self.load_extension(f"cogs.{filename[:-3]}")

    # Add generic commands that are not bound to our cogs
    def add_commands(self):
        # Command that will respond in case our bot is turned on
        @self.command(name="status", pass_context=True)
        async def status(ctx):
            await ctx.channel.send(Application._bot_status + " " + ctx.author.mention)